const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')
async function main(){
  const newinformaticsBuilding = await Building.findById('6218d79644e0aa02e39f5ae9')
  const room = await Room.findById('6218d79644e0aa02e39f5aee')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newinformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newinformaticsBuilding
  newinformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newinformaticsBuilding.save()
  informaticsBuilding.save()
}

main().then(() =>{
  console.log('Finish')
})