const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')
async function main(){
  //Update
  // const room = await Room.findById('6218d79644e0aa02e39f5aea')
  // room.capacity = 20
  // room.save()
  // console.log(room)
  const room = await Room.findOne({capacity: { $gte: 100}}).populate('building')
  console.log(room)
  console.log('------')
  const rooms = await Room.find({capacity: { $gte: 100}}).populate('building')
  console.log(rooms)
  const buildings = await Building.find({})
  console.log(JSON.stringify(buildings))
}

main().then(() =>{
  console.log('Finish')
})